# FLEXBOX

# Material to practice FLEXBOX:

[Flexbox Froggy](https://flexboxfroggy.com)

# Justify Content

This CSS property, aligns items **horizontally** and accepts the following values:

- **flex-start**: Items align to the left side of the container.
- **flex-end**: Items align to the right side of the container.
- **center**: Items align at the center of the container.
- **space-between**: Items display with equal spacing between them.
- **space-around**: Items display with equal spacing around them.

# Align Items

This CSS property, **vertically** aligns the elements and it accepts the following values:

- **flex-start**: Aligns elements to the top of its container.
- **flex-end**: Aligns elements to the bottom of its container.
- **center**: Aligns elements to the center (vertically speaking) of its container.
- **baseline**: Shows elements in the baseline of its container.
- **stretch**: The elements are tighten to adjust (vertically) to fit the container.

# Flex Basis

This prop allows us to give width to an element.
While display:flex works in the container, flex-basis is applied to their child to indicate the "width" every child should take from the container.

Example 1:

```
.d-flex-8 {
    display: flex;
}

.d-flex-8 .box {
    /* Every box will take 1/3 of the total witdth*/
    flex-basis: 33.3%;
}

```

Example 2:

```
.d-flex-8 {
    display: flex;
}

.d-flex-8 .box {
    /* Every box will take 200px UNLESS the content is bigger than 200px in which case it will grow*/
    flex-basis: 200px;
}

```

The difference between _flex-basis_ and _width_ is:

- flex-basis is an initial value, if the content is bigger, it will take the space that it needs.
- width will take the exact measure you indicate, no matter if the content fits or not.

# Differences between start and flex-start

The values **start, center, end** are defined in the CSS Box Alignment specification, and they are intended to **apply to multiple box layout models** (block, table, grid, flex, etc.).

While the other values (**flex-start, flex-end**) are defined in the CSS flexbox specification, and are intended to **apply only to flex layout**.

With the Box Alignment spec, the W3C is attempting to establish a universal language for the alignment of boxes in CSS. Eventually, the Box Alignment values will supersede the particular values defined in flex, grid and other specs.

For example:

- **end** will be used instead of **flex-end**
- **column-gap** will be used instead of **grid-column-gap**
- and so on.

Many Box Alignment values are already in use across major browsers. But full implementation is still a ways off, so it's still safer to use flex-end instead of end (and then count on long-term support for legacy names).

# Gap & Calc

If we want to create some space between columns we can use gap:

```
.d-flex-9 {
    display: flex;
    column-gap: 2rem;
    justify-content: center;
}


```

The problem: gap is not supported by all devices. (You can take a look in [CAN I USE?](https://caniuse.com/))
So, a common practice is to use calc() which is supported by 98% of devices:

- We calculate the width + the space in the flex-basis
- We spare the elements with justify-content: space-between

Example:

```
.d-flex-9 {
    display: flex;
    justify-content: space-between;
}

.d-flex-9 .box {
    flex-basis: calc(33.3% - 2rem);
}
```

# Flex Grow

The flex-grow property specifies how much the item will grow relative to the rest of the flexible items inside the same container. Including padding, margin, etc.

Example: the first child is getting 2px everytime, while the other children are getting 1px.

```
.d-flex-11 {
    display: flex;
}

.d-flex-11 .box:nth-child(1) {
    flex-grow: 2;
}

.d-flex-11 .box:nth-child(2) {
    flex-grow: 1;
}

.d-flex-11 .box:nth-child(3) {
    flex-grow: 1;
}
```

# Flex Shrink

The flex-shrink property specifies how the item will shrink relative to the rest of the flexible items inside the same container.

Example
Let the second flex-item shrink three times more than the rest:

```
div:nth-of-type(2) {
  flex-shrink: 3;
}

```
