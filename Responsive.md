# RESPONSIVE WEB DESIGN

# What is Responsive Web Design?

Responsive web design makes your web page look good on all devices.

It is called responsive web design when you use CSS and HTML to resize, hide, shrink, enlarge, or move the content to make it look good on any screen.

# Designing For The Best Experience For All Users

Web pages can be viewed using many different devices: desktops, tablets, and phones. Your web page should look good, and be easy to use, regardless of the device.

Web pages should not leave out information to fit smaller devices, but rather adapt its content to fit any device.

# Media Queries

Media query is a CSS technique introduced in CSS3.

It uses the @media rule to include a block of CSS properties only if a certain condition is true.

Example:

If the browser window is 600px or smaller, the background color will be lightblue:

```
@media only screen and (max-width: 600px) {
  body {
    background-color: lightblue;
  }
}
```

# To Test your Responsive Site

[Responsively App](https://responsively.app/download/)

# Media Queries - Always Design for Mobile First

Mobile First means designing for mobile before designing for desktop or any other device (This will make the page display faster on smaller devices).

This means that we must make some changes in our CSS.

Instead of changing styles when the width gets smaller than 768px, we should change the design when the width gets larger than 768px.

Example:
Have in mind the indicated props are rewritten, the other ones which are not indicated, remain equal.

```
.oferta {
  background-color: #00446e;
  padding: 2rem;
  text-align: center;
}

.precio {
  font-size: 4rem;
  margin: 0
}

<!-- this media query is ignore if the window size is smaller than 768px -->
@media(min-width: 768px) {
  .oferta {
    max-width: 40rem;
    margin: 0 auto
  }

<!-- Here, the margin whis is not indicated, will remain margin: 0 -->
  .precio {
    font-size: 2rem;
  }
}

```

```
  <div class="oferta">
      <p class="precio">$20 Descuento</p>
      <p class="texto">Utiliza el cupón: 20OFF al pagar</p>
  </div>
```

# Media Queries - Desktop First

Your start designing for big devices and Media Querias applies to smaller screens. In this approach, the order of media queries is very important, since a screen of 500px would apply for the two of them. And css works in cascade!.

Example:

```
.oferta {
  background-color: #00446e;
  padding: 2rem;
  text-align: center;
  max-width: 60rem;
  margin: 0 auto;
}

.precio {
  font-size: 4rem;
  margin: 0
}

@media(max-width: 600px) {
  .oferta {
    max-width: 40rem;
  }
}

@media(max-width: 800px) {
  .oferta {
    max-width: 100%;
  }
}
```

# Media queries between 2 sizes

```
@media (min-width: 600px) and (max-width: 800px) {
  .oferta {
    background-color: yellow;
    max-width: 30rem;
  }
}
```

In case we need to add more conditions:

```
@media (min-width: 600px) and (max-width: 800px), (min-width: 1200px) {
  .oferta {
    background-color: yellow;
    max-width: 30rem;
  }
}
```

# Creating Snippets for Media Queries

1. Ctrl + p
2. Write: >user snippets
3. Choose css.json
4. Create your snippet like this:

   ```
   {
     // Place your snippets for css here. Each snippet is defined under a snippet name and has a prefix, body and
     // description. The prefix is what is used to trigger the snippet and the body will be expanded and inserted. Possible variables are:
     // $1, $2 for tab stops, $0 for the final cursor position, and ${1:label}, ${2:another} for placeholders. Placeholders with the
     // same ids are connected.
     // Example:
     // "Print to console": {
     // 	"prefix": "log",
     // 	"body": [
     // 		"console.log('$1');",
     // 		"$2"
     // 	],
     // 	"description": "Log output to console"
     // }

     "media query": {
       "prefix": "mq",
       "body": ["@media (min-width: $1) { \n\t$2\n }"]
     }
   }
   ```

5. Use it in your css file! You can write 'mq' and press 'tab'

# Standards for Media Queries

In general:

```
// Extra small devices (portrait phones, less than 576px)
// No media query since this is the default in Bootstrap

// Small devices (landscape phones, 576px and up)
@media (min-width: 576px) { ... }

// Medium devices (tablets, 768px and up)
@media (min-width: 768px) { ... }

// Large devices (desktops, 992px and up)
@media (min-width: 992px) { ... }

// Extra large devices (large desktops, 1200px and up)
@media (min-width: 1200px) { ... }
```

For more detail visit: [CSS-TRICKS](https://css-tricks.com/snippets/css/media-queries-for-standard-devices/)

# Create Responsive Containers

```
.contenedor-responsive {
  background-color: white;

  width: 90%;
  max-width: 1000px;
  /*  Same thing in just one line:
  width: min(90%, 1000px); */

  height: 400px;
  margin: 0 auto;
}
```

# Responsive Columns in CSS in Flexbox

```
@media (min-width: 768px) {
  .tres-columnas-flex {
    display: flex;
    gap: 2rem;
  }

  .columna {
    flex: 1;
  }
}
```

```
<div class="tres-columnas-flex">
  <div class="columna">1</div>
  <div class="columna">2</div>
  <div class="columna">3</div>
</div>
```

# Responsive Columns in CSS in Grid

```
<div class="tres-columnas-grid">
  <div class="columna">1</div>
  <div class="columna">2</div>
  <div class="columna">3</div>
</div>
```

```
@media (min-width: 768px) {
.tres-columnas-grid {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  column-gap: 2rem;
  }
}
```

# Responsive Images

We use max-width instead of width so if the image is bad quality and smaller that container, it wont grow.

```
img {
  max-width: 100%;
  display: block;
}
```

```
<div class="image">
  <img src="img/imagen.jpg" alt="imagen responsive" />
</div>
```

# Images Avif & Webp for better performance

If we use the tag < img >, not many image formats are supported depending on the browser.
So we need to use < picture >, which will try to show the first format, and if not possible tryes the next one.

Example:

```
  <h2>Formatos Modernos</h2>
  <picture>
    <source  srcset="img/imagen.avif" type="image/avif"/>
    <source  srcset="img/imagen.webp" type="image/webp"/>
    <img loading="lazy" src="img/imagen.jpg" alt="imagen" />
  </picture>
```

For the SEO is a good idea for our web to load quickly. Once we open the web, our images will be downloaded, no matter the user still have to scroll to the bottom to see it.
Thats why we add the attribute loading="lazy" to the < img >, for the resource to load only when the user positionates to see it "on-demand".

# Creating Snippets for Avif & Webp

1. Ctrl + p
2. Write: >user snippets
3. Choose html.json
4. Create your snippet like this:

   ```
   {
   {
   // Place your snippets for html here. Each snippet is defined under a snippet name and has a prefix, body and
   // description. The prefix is what is used to trigger the snippet and the body will be expanded and inserted. Possible variables are:
   // $1, $2 for tab stops, $0 for the final cursor position, and ${1:label}, ${2:another} for placeholders. Placeholders with the
   // same ids are connected.
   // Example:
   // "Print to console": {
   // 	"prefix": "log",
   // 	"body": [
   // 		"console.log('$1');",
   // 		"$2"
   // 	],
   // 	"description": "Log output to console"
   // }
   "images": {
    "prefix": "im",
    "body": [
      "<picture>",
      "\t<source srcset=\"$1.avif\" type=\"image/avif\">",
      "\t<source srcset=\"$2.webp\" type=\"image/webp\">",
      "\t<img loading=\"lazy\" src=\"$3.jpg\" width=\"500\" height=\"300\" alt=\"$4\">",
      "</picture>"
    ]
   }
   }

   ```

5. Use it in your html file! You can write 'im' and press 'tab'

# Showing Images for Different Size With SRCSET

```
  <h2>Formatos Modernos con SRC SET</h2>
  <picture>
    <source
      sizes="1920w, 1280w, 640w"
      srcset="
        img/imagen.avif      1920w,
        img/imagen-1000.avif 1280w,
        img/imagen-700.avif   640w
      "
      type="image/avif"
    />
  </picture>
```
