# SELECTORS

# 1. Label Selector

```
p {
    font-size: 2rem;
    color: red;
}
```

# 2. Class Selector

```
.first-paragraph {
    font-size: 2rem;
    color: red;
}
```

# 3. Two Classes Selector

```
/*  applies to every element that has both classes .texto and .mayusculas (no matter the order)*/
.texto.mayusculas {
    font-size: 10rem;
}
```

```
<div class="codigo">
    <p class="texto">Primer Texto</p>
    <p class="texto azul mayusculas">Segundo Texto</p> <!--APPLIES TO THIS ONE! -->
    <p class="texto">3er Texto</p>
</div>
```

# 4. Nested Classes Selector

```
/* This will apply in case there is a class 'header' with a  n-child with class 'description' */
.header .descripcion {
    color: red;
}

.descripcion {
    color: blue;
}

```

```
<div class="codigo">
    <header class="header">
        <div class="contenedor1">
            <div class="descripcion">Descripción -- RED</div>
            <div class="contenedor2">
                <div class="descripcion">
                    <p>Parrafo -- RED</p>
                    <p>Otro Parrafo -- RED</p>
                </div>
            </div>
        </div>
    </header>
    <div class="descripcion">Segunda Descripción -- BLUE</div>
</div>
```

# 5. Id Selector

```
#encabezado {
    text-transform: uppercase;
    color: red;
}

.texto {
    color: blue;
}

#encabezado.texto {
    color: turquoise;
}
```

```
<div class="codigo">
    <p id="encabezado" class="texto">Primer Texto -- UPPERCASE TURQUOISE (x la especificidad)</p>
    <p class="texto">Segundo Texto -- BLUE</p>
</div>
```

# 6. Label & Class Selector

```
<!-- Will apply to a div which has a class 'text' -->
div.texto{
  background-color: yellow;
}
```

```
<div class="codigo">
    <p class="texto">Primer Texto</p>
    <p class="texto">Segundo Texto</p>
    <p class="texto">3er Texto</p>

    <div class="texto">
        Oferta Especial! -- YELLOW
    </div>
</div>
```

# 7. Select two elements (or more) for the same css code

```
.texto,
.oferta {
  border: 3px solid yellow;
  padding: 2rem;
}
```

```

<div class="codigo">
    <p class="texto">Primer Texto -- YELLOW</p>
    <p class="texto">Segundo Texto -- YELLOW</p>
    <p class="texto">3er Texto -- YELLOW</p>

    <div class="oferta">
        Oferta Especial! -- YELLOW
    </div>
</div>

```

# 8. First Level Children

```
<!-- this applies to the VERY FIRST div which is a child of an element with class .admin -->
.admin > div {
  border: 2px solid red;
}
```

```
<div class="codigo">
    <div class="admin">
        <div> <!-- Only will apply to THIS div -->
            <p>Has Iniciado sesión como: Admin</p>
            <div> <!-- Not this! -->
                <p>Cerrar Sesión aquí</p>
            </div>
        </div>
    </div>
</div>

```

# 9. First Element After

```
.admin div + p {
  background-color: red;
}
```

```
<div class="codigo">
    <div class="admin">
        <div>
            <p>Cerrar Sesión aquí --RED </p>
        </div>
        <p>Has Iniciado sesión como: Admin</p>
    </div>
</div>
```

# 10. Attribute

```
/* This will change just the link that has exactly this prop: href="http://www.google.com"

a[href="http://www.google.com"] {
  color: red;
} */

/* If I want to modify all links that START with href="http://" */
a[href^="http://"] {
  color: red;
}

/* If I want to modify all links that END with href=".com" */
a[href$=".com"] {
  color: tomato;
}
```

```
<div class="codigo">
    <nav>
        <a href="http://www.google.com">Google</a>
        <a href="tienda.html">Tienda</a>
    </nav>

    <input type="tel" id="telefono" value="1212" />
</div>
```

# 11 & 12 .First Child and Last Child in a List

```
/* ul li:first-child {
  border: 2px solid red;
} */

/* An alternative for the same: */
ul li:first-of-type {
  border: 2px solid red;
}
```

```
<div class="codigo">
    <p>Lista de Compras</p>
    <ul>
        <li>Compra 1</li>
        <li>Compra 2</li>
        <li>Compra 3</li>
        <li>Compra 4</li>
        <li>Compra 5</li>
    </ul>
</div>
```

# 13. Select an Specific Element

```
ul li:nth-child(2) {
  border: 2px solid red;
}

/* To select multiple elements:

ul li:nth-child(2),
li:nth-child(3),
li:nth-child(5) {
  border: 2px solid red;
}

ul li:nth-child(2n + 1){
  border: 2px solid red;
}

ul li:nth-child(4n + 4){
  border: 2px solid red;
}

ul li:nth-child(odd){
  border: 2px solid red;
}

ul li:nth-child(even){
  border: 2px solid red;
}
*/
```

```
<div class="codigo">
    <p>Lista de Compras</p>
    <ul>
        <li>Compra 1</li>
        <li>Compra 2</li>
        <li>Compra 3</li>
        <li>Compra 4</li>
        <li>Compra 5</li>
    </ul>
</div>
```

# 14. All elements Except one/ones

```
/*
.heading,
.descripcion,
.oferta {
  color: red;
} */

/* An alternative:
p:not(.texto){
  color: red;
} */

/*Select multiple: */
p:not(.texto):not(.oferta){
  color: red;
}
```

```
<div class="codigo">
    <div class="elementos">
        <p class="texto">Texto 1</p>
        <p class="heading">Texto 2</p>
        <p class="descripcion">Texto 3</p>
        <p class="oferta">Texto 4</p>
    </div>
</div>
```

# 15. First Line or First Letter

```
.primer-letra::first-letter {
  font-size: 20rem;
}

.primer-linea::first-line {
  font-size: 14rem;
}
```

```
<div class="codigo">
    <p class="primer-letra"> Primer parrafo de una noticia</p>
    <p class="primer-letra"> Primer parrafo de una noticia</p>

    <p class="primer-linea">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Commodi, quis repellat
        quae laborum libero quod nemo placeat alias inventore sit optio dolores, doloribus possimus, provident
        eligendi beatae asperiores quam maiores!</p>
</div>
```
